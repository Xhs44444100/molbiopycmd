#!/usr/bin/python3
import ctypes
import time
import os
ctypes.windll.kernel32.SetConsoleTitleW("分子生物学字符模拟 0.0.0.18a")
varn = True
def cmdmain():
    print("@********************************@")
    print("|   分 子 生 物 学 字 符 模 拟   |")
    print("|help : 帮助                     |")
    print("|about: 关于                     |")
    print("|exit : 退出                     |")
    # 格式化成2016-03-20 11:45:39这样的形式
    print("|现在时间为:",time.strftime("%Y-%m-%d %H:%M:%S |", time.localtime()))
    print("@********************************@")

def help2():
    # 缩写解释
    print("╔---------------------------------------------------------------╗")
    print(" DNA:脱氧核糖核酸；RNA:核糖核酸")
    print(" A:腺苷；G:鸟苷；C:胞苷；U:尿苷")
    print(" dA:脱氧腺苷；dG:脱氧鸟苷；dC:脱氧胞苷；dT:脱氧胸苷")
    print(" 碱基缩写:A为腺嘌呤；G为鸟嘌呤；C为胞嘧啶；T为胸腺嘧啶；U为尿嘧啶")
    print(" p:磷酸基团")
    print(" pol:聚合酶")
    print(" ***...:其他酶（相对不重要的）")
    print("╚---------------------------------------------------------------╝")

def help3():
    # 全部参考来源
    print("╔-----------------------------------------------------╗")
    print(" 主要参考来源:《分子生物学（第二版）》")
    print(" 次要:")
    print(" 《分子生物学导论》（记忆中的）")
    print(" 中文维基百科（zh.wikipedia.org）条目")
    print("                   可能出现理论错误！")
    print("╚-----------------------------------------------------╝")

def test():
    print("\033[33m test TEST 加载中.................\033[0m")
    Add_swab = 1
    while Add_swab <= 20:
        print("█",end="")
        Add_swab = Add_swab + 1
        time.sleep(1)
    testr = ["","",""]
    testr[0] = input("test 1 :")
    testr[1] = input("test 2 :")
    testr[2] = input("test 3 :")
    print("TEST: p",testr[0],"p",testr[1],"p",testr[2])

def hello6():
       import urllib.request #引入urllib库
       we=input("搜索(a),知网(b):")
       if we=="a":
         wh=input("搜索:")
         print("由于谷歌学术有人机验证所以不支持")
         ww=input("微软学术(a)维基百科(b):")
         if ww=="a":
            response = urllib.request.urlopen("https://cn.bing.com/academic/search?q=,wh")  #微软学术
            html = response.read().decode('utf-8')
            print(html)
         if ww=="b":
               response = urllib.request.urlopen("https://zh.wikipedia.org/wiki/,wh")  #维基百科
               html = response.read().decode('utf-8')
               print(html)
       if we=="b":
            response = urllib.request.urlopen("https://www.cnki.net/")  #发出请求并且接收返回文本对象
            html = response.read().decode('utf-8')
            print(html)

ui = "[MBpy:\]" #ui main
cmdmain()
while varn == True:
    cin = input(ui)
    if cin == "new ui":
        nm = input("设置命令提示（old:生物学字符版，new:Xhs分子生物学pycmd版）:")
        if nm == "old":
            ui = "生物学_字符\>:$"
        else:
            ui = "[MBpy:\]"
    elif cin == "cmd":
        cmdmain()
    elif cin == "help":
        print("╔----------------------------------------╗")
        print(" cmd:标题页面")
        print(" help:帮助")
        print(" help2:缩写解释")
        print(" exit:退出")
        print(" about:关于")
        print(" help3:全部参考来源")
        print(" np:（“上网查资料”，来自《生物学-字符》）")
        print("               *注意大小写*")
        print("╚----------------------------------------╝")
    elif cin == "help2":
        help2()
    elif cin == "np":
        hello6()
    elif cin == "about":
        print("╔------------------------------------------------╗")#about
        print(" 分子生物学字符模拟 极早期开发:0.0.0.18a;")
        print(" 创作:Xhs44444100;")
        print(" 根据《分子生物学（第二版）》，但可能出现理论错误;")
        print(" （全部参考来源请输入 help3 ）")
        print(" 基于Python 3.9;")
        print(" 本软件采用CC BY-SA 4.0协议授权。")
        print("╚------------------------------------------------╝")
    elif cin == "help3":
        help3()
    elif cin == "test":
        test()
    elif cin == "exit":
            exitn = input("真的要退出吗？【n/y】:")
            if exitn == "y":
                varn = False
    else:
        if cin != "":
            print("未知指令!")


#创作：Xhs44444100；
#本程序采用CC BY-SA 4.0协议授权。