**本程序已转移到[Gitee上](https://gitee.com/xhs44444100/molbiopycmd/tree/master)。Bitbucket上不再更新。** 

**本程序采用CC BY-SA 4.0协议授权。**

**本程序目前还在极早期开发阶段，没有功能（但在 0.0.0.17a 后可输入“test”进入测试功能）**

**作者只是一个初中生，程序可能出现理论错误，请谅解。**

**分子生物学字符模拟**是我写的一个模拟**分子生物学的中心法则**的程序。

本程序的前身是我于2020年开发的《生物学-字符》（有早期版本发布在[B站](https://bilibili.com)动态上），但这个程序在我重装系统后丢失了，只保留了几个中期版本。于是，我重新写了一个程序《Xhs分子生物学pycmd》，后于0.0.0.12a版本改名为现在这个名字。

因为本程序的开发进程与我阅读《分子生物学（第二版）》的进度相仿，因此本程序有点**读书笔记**的意义。

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---

## 版本列表

“Xhs分子生物学pycmd”：0.0.0.1a ~ 0.0.0.12a

“分子生物学字符模拟”：

极早期开发：0.0.0.12a，0.0.0.13a，0.0.0.13b，0.0.0.14a（0.0.0.13c），0.0.0.15a，0.0.0.15b，0.0.0.15c，0.0.0.16a，0.0.0.17a，0.0.0.18a